package com.carrotrocket.nearbycafesrestaurants;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Loader;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.carrotrocket.nearbycafesrestaurants.model.Venue;
import com.carrotrocket.nearbycafesrestaurants.tasks.GetLatLngFromAddressTask;
import com.carrotrocket.nearbycafesrestaurants.utils.C;
import com.carrotrocket.nearbycafesrestaurants.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks {

    private GoogleMap mMap;
    private EditText et_search;
    private final int LOADER_LATLNG = 1;
    private static final String TAG = "MainActivity";
    private ProgressDialog pDialog;
    private String radius;
    private List<Venue> marks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        et_search = (EditText) findViewById(R.id.et_search_address);
        radius = getRadiusFromSettings(Utils.getIntPreference(this, C.RADIUS, 0));

        getLoaderManager().initLoader(LOADER_LATLNG, null, MainActivity.this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (et_search.getText().toString() != null & !et_search.getText().toString().isEmpty()) {
            Bundle bndl = new Bundle();
            bndl.putString(C.ADDRESS, et_search.getText().toString());
            bndl.putString(C.RADIUS, radius);
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
            getLoaderManager().restartLoader(LOADER_LATLNG, bndl, MainActivity.this).forceLoad();

        } else {
            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-34, 151);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (pDialog != null)
            pDialog.dismiss();
        pDialog = null;
    }

    public void onSearchAddress(final View view) {
        Utils.hideKeyboard(MainActivity.this, et_search);
        Bundle bndl = new Bundle();
        bndl.putString(C.ADDRESS, et_search.getText().toString());
        bndl.putString(C.RADIUS, radius);
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
        getLoaderManager().restartLoader(LOADER_LATLNG, bndl, MainActivity.this).forceLoad();
    }

    public void onSetRadius(final View view) {

        String data[] = {"100 m", "200 m", "500 m", "1000 m"};
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle(R.string.select_radius);
        //if not setting check 0 item
        ad.setSingleChoiceItems(data, Utils.getIntPreference(MainActivity.this, C.RADIUS, 0), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        ad.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        ad.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ListView lv = ((AlertDialog) dialog).getListView();
                Utils.setIntPreference(MainActivity.this, C.RADIUS, lv.getCheckedItemPosition());
                radius = getRadiusFromSettings(lv.getCheckedItemPosition());
                Utils.logD("KHJKGHKJG", "------" + radius);
                dialog.dismiss();
            }
        });
        ad.show();
    }

    private String getRadiusFromSettings(final int position) {
        switch (position) {
            case 0:
                return "100";
            case 1:
                return "200";
            case 2:
                return "500";
            case 3:
                return "1000";
        }
        return "100";
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Utils.logD(TAG, "onCreateLoader");
        Loader<List<Venue>> loader = null;
        if (id == LOADER_LATLNG) {
            loader = new GetLatLngFromAddressTask(this, args);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        Utils.logD(TAG, "onLoadFinished");
        if (loader.getId() == LOADER_LATLNG) {
            if (pDialog != null)
                pDialog.dismiss();
            pDialog = null;
            if (data == null) {
                et_search.setText(R.string.no_data);
            } else {
                marks = (List<Venue>) data;
                if (marks.size() > 0) {
                    setMark(marks.get(0).getLat(), marks.get(0).getLng(), marks.get(0).getName());
                    for (int i = 1; i < marks.size(); i++) {
                        Utils.logD(TAG, "lat = " + marks.get(i).getLat() + ", lng = " + marks.get(i).getLng());
                        Utils.logD(TAG, "name = " + marks.get(i).getName() + ", photo = " + marks.get(i).getPhoto());
                        setMarks(marks.get(i).getLat(), marks.get(i).getLng(), marks.get(i).getName(), marks.get(i).getPhoto());
                    }
                } else et_search.setText(R.string.no_data);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private void setMark(final double lat, final double lng, final String name) {
        LatLng searchOrMyPosition = new LatLng(lat, lng);
        if (mMap != null) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(searchOrMyPosition).title(name));
            if (radius.equals("100"))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchOrMyPosition, 17));
            if (radius.equals("200"))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchOrMyPosition, 16));
            if (radius.equals("500"))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchOrMyPosition, 15));
            if (radius.equals("1000"))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchOrMyPosition, 14));
        }
    }

    private void setMarks(final double lat, final double lng, final String name, final String photo) {
        LatLng latLng = new LatLng(lat, lng);
        if (mMap != null) {
            mMap.addMarker(new MarkerOptions().position(latLng).title(name)).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                // Use default InfoWindow frame
                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                // Defines the contents of the InfoWindow
                @Override
                public View getInfoContents(Marker arg0) {

                    View v = getLayoutInflater().inflate(R.layout.infoviewlayout, null);

                    LatLng latLng = arg0.getPosition();

                    ImageView im = (ImageView) v.findViewById(R.id.imageView1);
                    TextView tv1 = (TextView) v.findViewById(R.id.textView1);
                    String title = arg0.getTitle();
                    tv1.setText(title);
                    for (int i = 1; i < marks.size(); i++) {
                        if (marks.get(i).getLat() == latLng.latitude & marks.get(i).getLng() == latLng.longitude)
                            im.setImageBitmap(BitmapFactory.decodeFile(marks.get(i).getPhoto()));
                    }
                    return v;

                }
            });
        }

    }

}
