package com.carrotrocket.nearbycafesrestaurants.tasks;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;

import com.carrotrocket.nearbycafesrestaurants.model.Venue;
import com.carrotrocket.nearbycafesrestaurants.utils.C;
import com.carrotrocket.nearbycafesrestaurants.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Zaur on 30.01.2016.
 */
public class GetLatLngFromAddressTask extends AsyncTaskLoader<List<Venue>> {
    private String address;
    private String radius;
    private static final String TAG = "GetLatLngFromAddressTask";
    private Context context;

    public GetLatLngFromAddressTask(Context context, Bundle args) {
        super(context);
        this.context = context;
        if (args != null) {
            address = args.getString(C.ADDRESS);
            radius = args.getString(C.RADIUS);
            Utils.logD("KHJKGHKJG",radius);
        }
    }

    @Override
    public List<Venue> loadInBackground() {
        String responseLngLat;
        String responseMarks;
        List<Venue> result = new ArrayList<Venue>();
        if (TextUtils.isEmpty(address)) {
            return null;
        }
        try {
            responseLngLat = getResponse(C.GOOGLE_MAPS_GEOCODE + Uri.encode(address));
            Utils.logD(TAG, "responseLngLat = " + responseLngLat);
            JSONObject jsonObject = new JSONObject(responseLngLat);
            double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");
            double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");
            Utils.logD(TAG, "lat = " + lat);
            Utils.logD(TAG, "lng = " + lng);
            String url_fourSquare = C.FOURSQUARE_VENUES + "&radius=" + radius + "%20&intent=browse&v=20160131&ll=" + String.valueOf(lat) + "," + String.valueOf(lng);
            Utils.logD(TAG, "url_fourSquare = " + url_fourSquare);
            responseMarks = getResponse(url_fourSquare);
            Utils.logD(TAG, "responseMarks = " + responseMarks);
            result = parseFoursquare(responseMarks);

            //result = lat + "," + lng;
            result.add(0, new Venue(lat, lng, "select position", null));
        } catch (JSONException e) {
            if (context != null)
                e.printStackTrace();
        } catch (Exception e) {
            if (context != null)
                e.printStackTrace();
        }
        return result;
    }

    private String getResponse(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(21000);
            conn.setConnectTimeout(21000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            /*
            NOT USED IF responseCode == 200 with GET
             */
            //conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            Utils.logD(TAG, "responseCode = " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
            Utils.logD(TAG, e.getMessage());
        }
        return response;
    }

    private List<Venue> parseFoursquare(final String response) {

        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Utils.logE(TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return null;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" + "images");
        // создаем каталог, проверив его существование
        if (!sdPath.isDirectory())
            sdPath.mkdirs();

        List<Venue> marks = new ArrayList<Venue>();
        try {
            // make an jsonObject in order to parse the response
            JSONObject jsonObject = new JSONObject(response);
            // make an jsonObject in order to parse the response
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("venues")) {
                    JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("venues");
                    String id;
                    String name;
                    double lat;
                    double lng;
                    Utils.logD(TAG, "count nearby = " + jsonArray.length());
                    String photo = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        id = jsonArray.getJSONObject(i).getString("id");
                        lat = jsonArray.getJSONObject(i).getJSONObject("location").getDouble("lat");
                        lng = jsonArray.getJSONObject(i).getJSONObject("location").getDouble("lng");
                        name = jsonArray.getJSONObject(i).getString("name");
                        Utils.logD(TAG, "id = " + id);
                        Utils.logD(TAG, "lat = " + lat);
                        Utils.logD(TAG, "lng = " + lng);
                        Utils.logD(TAG, "name = " + name);
                        //limit = 1, we need 1 photo
                        photo = null;
                        String urlResponsePhoto = C.FOURSQUARE_PHOTO + id + "/photos?v=20160131&limit=1&client_id=" + C.CLIENT_ID +
                                "&client_secret=" + C.CLIENT_SECRET;
                        //Utils.logD(TAG, "urlResponsePhoto = "+urlResponsePhoto);
                        String responsePhoto = getResponse(urlResponsePhoto);
                        Utils.logD(TAG, "responsePhoto=" + responsePhoto);
                        JSONObject jsonPhotos = new JSONObject(responsePhoto);
                        //search object with field count == 1
                        if (jsonObject.has("response")) {
                            if (jsonPhotos.getJSONObject("response").has("photos")) {
                                if (jsonPhotos.getJSONObject("response").getJSONObject("photos").getInt("count") == 1) {
                                    if (jsonPhotos.getJSONObject("response").getJSONObject("photos").has("items")) {
                                        JSONArray jsonArrayPhotos = jsonPhotos.getJSONObject("response").getJSONObject("photos").getJSONArray("items");
                                        photo = ((JSONObject) jsonArrayPhotos.get(0)).getString("prefix") + "100x100" + ((JSONObject) jsonArrayPhotos.get(0)).getString("suffix");
                                    }
                                }
                            }
                            Utils.logD(TAG, "photo = " + photo);
                            if (photo != null) {
                                String file = sdPath +"/"+ photo
                                        .substring(photo.lastIndexOf("/") + 1);
                                Utils.DownloadFile(photo, file);
                                marks.add(new Venue(lat, lng, name, file));
                            } else marks.add(new Venue(lat, lng, name, null));
                        }

                    }
                }
            }
        } catch (JSONException e) {
            if (context != null)
                e.printStackTrace();
        }
        return marks;
    }
}
