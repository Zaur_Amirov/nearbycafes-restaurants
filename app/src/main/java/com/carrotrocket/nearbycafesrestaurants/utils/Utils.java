package com.carrotrocket.nearbycafesrestaurants.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.carrotrocket.nearbycafesrestaurants.App;
import com.carrotrocket.nearbycafesrestaurants.BuildConfig;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Zaur on 30.01.2016.
 */
public class Utils {

    private static final String TAG = "Utils";

    public static void hideKeyboard(final Context context, final View view) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getString(int id) {
        return App.getInstance().getString(id);
    }

    /**
     * Log.d
     *
     * @param tag
     * @param msg
     */
    public static void logD(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Debug" : msg);
        }
    }

    /**
     * DBGE == Log errors(Log.e)
     *
     * @param tag
     * @param msg
     */
    public static void logE(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Error!" : msg);
        }
    }

    public static int getIntPreference(Context ctx, String key, int def) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
        return settings.getInt(key, def);
    }

    public static void setIntPreference(Context ctx, String key, int value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void DownloadFile(String imageURL, String fileName) {
        try {
            URL url = new URL(imageURL);
            File file = new File(fileName);
            URLConnection ucon = url.openConnection();
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bis.read()) != -1)
                baf.append((byte) current);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.close();

        } catch (Exception e) {
			Utils.logE(TAG, "Error download image");
        }
    }
}
