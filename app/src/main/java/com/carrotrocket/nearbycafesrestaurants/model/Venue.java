package com.carrotrocket.nearbycafesrestaurants.model;

/**
 * Created by Zaur on 31.01.2016.
 */
public class Venue {
    private double lat;
    private double lng;
    private String name;
    private String photo;

    public Venue(final double lat, final double lng, final String name, final String photo){
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }
}
